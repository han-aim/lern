#!/bin/sh
set -eux
timestamp_system="$(date -Iseconds)"
edition="${CI_COMMIT_TAG:-$REVISION_GIT}"
timestamp="${CI_COMMIT_TIMESTAMP:-$timestamp_system}"
printf '%s' "$edition" >version.txt
printf '%s' "$timestamp" >timestamp.txt
