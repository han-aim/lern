#!/bin/sh
set -eux
pnpm \
  build
apk \
  del \
  --no-network -- \
  .buildonly
rm \
  -fr \
  /var/cache/apk/*
