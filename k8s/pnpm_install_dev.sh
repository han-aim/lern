#!/bin/sh
set -eux
pnpm \
  install \
  --frozen-lockfile \
  --strict-peer-dependencies
