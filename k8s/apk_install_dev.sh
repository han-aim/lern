#!/bin/sh
set -eux
echo 'http://dl-cdn.alpinelinux.org/alpine/edge/testing' \
  >>'/etc/apk/repositories' &&
  apk \
    add \
    --no-cache -- \
    git~=2.39 \
    mkcert~=1.4 \
    openjdk17-jre-headless~=17.0 \
    rsync~=3.2 \
    shfmt~=3.6
rm \
  -rf \
  /var/cache/apk/*
mkdir -- \
  ~lern/.config/
git \
  config \
  --system \
  --add \
  safe.directory "$(pwd)"
