#!/bin/sh
set -eux
sed \
  -i \
  -e 's/v[[:digit:]]\..*\//edge\//g' '/etc/apk/repositories'
apk \
  add \
  --no-cache -- \
  nodejs-current~=19.3 \
  npm~=9.2 \
  tini~=0
apk \
  add \
  --no-cache \
  --virtual .buildonly -- \
  curl~=7.86 \
  git~=2.39
ln \
  -s -- \
  ~/.profile ~/.bashrc
npm \
  install \
  -g \
  'pnpm@7.17'
env \
  SHELL=/bin/bash \
  pnpm \
  setup
# shellcheck source=/dev/null
. ~/.profile
addgroup \
  -g 10001 \
  -S lern
adduser \
  -u 10001 \
  -S \
  -D \
  -G lern lern
