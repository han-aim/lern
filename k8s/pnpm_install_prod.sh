#!/bin/sh
set -eux
pnpm \
  install \
  --frozen-lockfile \
  --prod \
  --strict-peer-dependencies
