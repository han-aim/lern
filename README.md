<!-- LTeX: language=en -->
<!-- omit in toc -->
# The Lern e-learning content management system

- [Content authoring](#content-authoring)
  - [Advantages of Pug over HTML](#advantages-of-pug-over-html)
  - [Advantages of HTML/CSS and Pug over Markdown](#advantages-of-htmlcss-and-pug-over-markdown)
- [Development](#development)
  - [To set up development environment](#to-set-up-development-environment)
    - [Build Lern’s container image](#build-lerns-container-image)
    - [Deploy Lern](#deploy-lern)
    - [Set up Git-based development inside the deployed Lern](#set-up-git-based-development-inside-the-deployed-lern)
    - [Make VS Code use the Pod](#make-vs-code-use-the-pod)
  - [To update Lern](#to-update-lern)
  - [To configure](#to-configure)
  - [To run Lern for development purposes](#to-run-lern-for-development-purposes)
  - [To build static Lern website for production](#to-build-static-lern-website-for-production)

## Content authoring

The `.pug` files are [Pug](https://pugjs.org/language/tags.html) templates.
Split up large HTML documents.
Use the web components defined in this projects for standard information parts, such as `<component-section>`.

### Advantages of Pug over HTML

- Supports conditional logic.
- Supports reuse of fragments.
- Less obtrusive syntax.
- Supports variables, including environment variables.
That supports conditional rendering with a CI pipeline without server-side support.
Example use cases: instructor versus student editions.
- Is rendered to optimized HTML, e.g., without comments.

### Advantages of HTML/CSS and Pug over Markdown

- Syntax and semantics specified in a standard.
- Supports more types of content elements.
With Markdown, the author has to fall back to HTML for anything beyond basic document formatting.
  - Markdown does not have complete and out-of-the-box support for tables.
  - Markdown does not have interactive elements or embeddable, enhancing elements.
- Being proficient at writing HTML and understanding CSS is more of a transferable skill than being so at writing Markdown.
E.g., content management systems require authors to understand basic HTML for more advanced editing.

## Development

### To set up development environment

Use [Rancher Desktop](https://rancherdesktop.io/) to deploy a Kubernetes namespace with the development environment (a dev container).

- Use the `containerd` container engine.

#### Build Lern’s container image

Based on your clone of the repository, build a container image for Lern.
This image will contain a functional version of Lern plus development tools.

On your workstation 💻:

```sh
nerdctl \
  --namespace k8s.io \
  build \
    --build-arg REVISION_GIT="$(git rev-parse --short HEAD)" \
    --cache-from lern-dev:latest \
    --file=k8s/Containerfile \
    --tag lern-dev:latest \
    --target=development \
    .
```

#### Deploy Lern

Deploy Lern and all required Kubernetes resources.

On your workstation 💻:

```sh
envsubst '$HOME' <k8s/kubectl-dev.yml | \
  kubectl \
    apply \
      --filename=-
```

Make Lern reachable as a network service on your workstation.

On your workstation 💻:

```sh
kubectl \
  port-forward \
    service/lern-dev-http \
    5173
```

#### Set up Git-based development inside the deployed Lern

Open a debug shell (🐜).

On your workstation 💻:

```console
$ kubectl \
  debug \
    --namespace=lern \
    --image=lern-dev:latest \
    --image-pull-policy=Never \
    --target=lern-dev \
    --stdin=true \
    --tty=true \
    "$(kubectl get pods --selector=app=lern-dev --output=name)" -- \
    sh
```

Start an Rsync server that can access the deployed Lern container’s filesystem.

In the debug shell 🐜:

```sh
rsync \
  --daemon
```

On your workstation 💻:

```console
$ kubectl \
  --namespace=lern \
  port-forward \
    "$(kubectl get pods --selector=app=lern-dev --output=name)" \
    8730:873
```

Transfer the Git data from the directory on your workstation to the root of Lern.

On your workstation 💻:

```sh
rsync \
  --progress \
  --recursive \
  .git/ \
  rsync://localhost:8730/srv-lern/.git/
```

Create a shell to the main container 🫙.

On your workstation 💻:

```sh
kubectl \
  --namespace=cook3rsiminterface \
  exec \
    --stdin=true \
    --tty=true \
    "$(kubectl get pods --selector=app=cook3rsiminterface-dev --output=name)" \
    -- \
    sh
```

You may now exit the debug shell 🐜.

In the shell container 🫙:

```sh
cd /srv/lern
git \
  restore \
    --overlay \
    .
```

#### Make VS Code use the Pod

Switch to the `lern` Kubernetes Namespace in VS Code starting by right-clicking the Namespace in the Kubernetes panel.
Attach VS Code to the container under the Deployment, again starting by right-clicking the Deployment.

![Attach VS Code to this container](media/Attach_Visual_Studio_Code.png)

<!-- TODO: make VS Code open the working directory automatically -->
Then, open the folder `/srv/lern/` in attached VS Code window.

Install all recommended VS Code extensions inside the attached window, as proposed by VS Code in popups.

### To update Lern

[Build Lern’s container image](#build-lerns-container-image) anew.
Kubernetes will update the Deployment.
To monitor this process, use regular Kubernetes tools or Rancher Desktop’s dashboard.

### To configure

Out of the box, the Vite dev server uses HTTPS.
A TLS certificate and key for localhost for development purposes are included.

To generate a new TLS certificate and key, run:

```sh
env TRUST_STORES= \
JAVA_HOME= \
  mkcert
    -ecdsa
    localhost.localdomain localhost 127.0.0.1 ::1
```

Trust the CA certificate on your client.
Its path is given by:

```sh
mkcert \
  -CAROOT
```

### To run Lern for development purposes

```sh
pnpm \
  dev
```

You normally do not need to issue this command.
Lern is already running automatically inside its container.

### To build static Lern website for production

```sh
pnpm \
  build
```
