// TODO: use error info.
const errorhandler = (_message, _file, _line, _col, _error) => {
  document.body.innerText = '❌ An error occurred. Use the developer console to debug.';
  return false;
};

// TODO: Error handler does not work for errors inside promises.
window.onerror = errorhandler;
window.addEventListener('unhandledrejection', errorhandler);
