import { LitElement, html, css } from 'lit';

class Gradingcriteria extends LitElement {
  static finalizeStyles(_styles) {
    return [
      css`
        *,
        ::before {
          box-sizing: border-box;
        }

        :host > div {
          min-width: 100%;
          max-width: fit-content;
          padding: 5px;
          background-color: var(--color-extra-bg);
          margin-block-end: 10px;
          margin-block-start: 10px;
        }

        :host > div::after {
          content: '🧭';
          position: relative;
          right: 10px;
          bottom: 0;
          font-size: 2rem;
        }
      `,
    ];
  }

  render() {
    return html`<div>
      <ol>
        <slot></slot>
      </ol>
    </div>`;
  }
}

export const componentGradingcriteria = () => {
  customElements.define('component-gradingcriteria', Gradingcriteria);
};
