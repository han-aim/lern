import { LitElement, html, css } from 'lit';

class Heading extends LitElement {
  constructor() {
    super();
    this.level = 0;
    this.index = '';
  }

  static get properties() {
    return { level: { type: Number }, index: { type: String } };
  }

  static finalizeStyles(_styles) {
    return [
      css`
        *,
        ::before {
          box-sizing: border-box;
        }

        .locator {
          display: block;
          margin-right: 10px;
          color: var(--color-numbering-fg);
          text-align: left;
          text-overflow: ellipsis;
          white-space: nowrap;
        }

        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
          display: inline-flex;
          align-items: baseline;
          padding-right: 10px;
        }

        h1 {
          margin-block: 3px;
          font-size: 2.25rem;
        }

        h2 {
          margin-block: 2px;
          font-size: 2rem;
        }

        h3 {
          margin-block: 1px;
          font-size: 1.7rem;
        }

        h4 {
          margin-block: 1px;
          font-size: 1.5rem;
        }

        h5 {
          margin-block: 1px;
          font-size: 1.25rem;
        }

        h6 {
          margin-block: 1px;
          font-size: 1.15rem;
        }
      `,
    ];
  }

  render() {
    let locator = '';
    if (this.index && this.level > 1 && this.level <= 6) {
      locator = html`<small class="locator"><a part="a" href="#sec_${this.index}">${this.index}</a></small> `;
    }
    const content = html`<slot></slot>`;
    switch (this.level) {
      case 1:
        return html`<h1>${locator}${content}</h1>`;
      case 2:
        return html`<h2>${locator}${content}</h2>`;
      case 3:
        return html`<h3>${locator}${content}</h3>`;
      case 4:
        return html`<h4>${locator}${content}</h4>`;
      case 5:
        return html`<h5>${locator}${content}</h5>`;
      case 6:
        return html`<h6>${locator}${content}</h6>`;
      default:
        return html`${locator}<strong>${content}</strong>`;
    }
  }
}

export const componentHeading = () => {
  customElements.define('component-heading', Heading);
};
