import { LitElement, html, css } from 'lit';

class Intendedlearningoutcomes extends LitElement {
  static finalizeStyles(_styles) {
    return [
      css`
        *,
        ::before {
          box-sizing: border-box;
        }

        :host {
          line-height: 1;
        }
      `,
    ];
  }

  render() {
    return html`<ol>
      <slot></slot>
    </ol>`;
  }
}

export const componentIntendedlearningoutcomes = () => {
  customElements.define('component-intendedlearningoutcomes', Intendedlearningoutcomes);
};
