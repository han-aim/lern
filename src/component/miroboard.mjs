import { LitElement, html, css } from 'lit';

class Miroboard extends LitElement {
  static get properties() {
    return {
      viewport: { type: String },
      idBoard: { type: String },
      title: { type: String },
    };
  }

  static finalizeStyles(_styles) {
    return [
      css`
        :host > iframe {
          overflow: auto;
          width: 100%;
          height: 100%;
          resize: vertical;
        }
      `,
    ];
  }

  render() {
    return html`<iframe
      src="https://miro.com/app/live-embed/${this.idBoard}&equals;/?moveToViewport&equals;${this.viewport}"
      allowfullscreen
      title="${this.title}"></iframe>`;
  }
}

export const componentMiroboard = () => {
  customElements.define('component-miroboard', Miroboard);
};
