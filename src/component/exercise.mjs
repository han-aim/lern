import { LitElement, html, css } from 'lit';

class Exercise extends LitElement {
  static get properties() {
    return { title: { type: String }, together: { type: Boolean }, extra: { type: Boolean } };
  }

  static finalizeStyles(_styles) {
    // TODO: inherit styles
    return [
      css`
        *,
        ::before {
          box-sizing: border-box;
        }

        :host > div {
          min-width: 100%;
          max-width: fit-content;
          margin: 10px;
          padding: 5px;
          background-color: var(--color-exercise-bg);
        }

        :host > div::after {
          content: '😅';
          position: relative;
          right: 10px;
          bottom: 0;
          font-size: 2rem;
        }

        :host([together]) > div::after {
          content: '😅👥';
        }

        :host([extra]) > div::after {
          content: '😅🧐';
        }

        :host([together][extra]) > div::after {
          content: '😅🧐👥';
        }
      `,
    ];
  }

  render() {
    return html`<div><slot></slot></div>`;
  }
}

export const componentExercise = () => {
  customElements.define('component-exercise', Exercise);
};
