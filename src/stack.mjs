export class Stack {
  constructor() {
    this.nItem = [];
  }

  push(item) {
    this.nItem.push(item);
  }

  pop() {
    return this.nItem.pop();
  }

  peek() {
    return this.nItem[this.nItem.length - 1];
  }

  get size() {
    return this.nItem.length;
  }

  [Symbol.iterator]() {
    return this;
  }

  next() {
    const item = this.pop();
    if (item) {
      return { done: false, value: item };
    }
    return { done: true };
  }
}
