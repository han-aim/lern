import CSL from 'citeproc';

export const processCitations = (citationData, locale, citationstyleIeee) => {
  const itemIDs = [];
  const citations = {};
  window.citations = citations;
  for (const elCitation of document.getElementsByTagName('cite')) {
    for (const itemCitation of citationData.items) {
      if (itemCitation.id === elCitation.dataset.id) {
        citations[itemCitation.id] = itemCitation;
        itemIDs.push(itemCitation.id);
        break;
      }
    }
  }
  const citeprocSys = {
    retrieveLocale: () => locale,
    retrieveItem: id => citations[id],
  };
  const processorOutput = () => {
    // eslint-disable-next-line no-use-before-define
    citeproc.updateItems(itemIDs);
    // eslint-disable-next-line no-use-before-define
    const result = citeproc.makeBibliography();
    return result[1].join('');
  };
  const dynamicCitations = () => {
    const citationsPre = [];
    const citationsPost = [];
    let counterCitation = 0;
    for (const elementCite of document.getElementsByTagName('cite')) {
      const citeitem = {
        id: elementCite.dataset.id,
        locator: elementCite.dataset.locator,
        label: elementCite.dataset.label,
        suffix: elementCite.dataset.suffix,
      };
      const citation = {
        citationItems: [citeitem],
        properties: {
          noteIndex: 0,
        },
      };
      // eslint-disable-next-line no-use-before-define
      citeproc.appendCitationCluster(citation);
      // eslint-disable-next-line no-use-before-define
      const citationtext = citeproc.previewCitationCluster(citation, citationsPre, citationsPost, 'html');
      citationsPre.push([citation.citationID, counterCitation]);
      counterCitation += 1;
      elementCite.insertAdjacentText('afterbegin', citationtext);
      const elementLink = document.createElement('a');
      // eslint-disable-next-line no-use-before-define
      for (const ref of citeproc.registry.reflist) {
        if (ref.id === citeitem.id) {
          elementLink.setAttribute('href', `#biblio_${ref.seq}`);
          break;
        }
      }
      elementCite.parentNode.insertBefore(elementLink, elementCite);
      elementLink.appendChild(elementCite);
    }
  };
  const citeproc = new CSL.Engine(citeprocSys, citationstyleIeee, 'nl-NL');
  citeproc.opt.development_extensions.wrap_url_and_doi = true;
  const elementBibliography = document.getElementById('bibliography');
  elementBibliography.insertAdjacentHTML('afterbegin', processorOutput());
  let countBiblio = 1;
  for (const elementReference of document.querySelectorAll('.csl-entry')) {
    elementReference.setAttribute('id', `biblio_${countBiblio}`);
    countBiblio += 1;
  }
  dynamicCitations();
};
