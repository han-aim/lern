// TODO: remove
export const renderLesson = elementBody => {
  const nElementLessonSummary = elementBody.querySelectorAll(':scope [data-lesson]');
  let counterLesson = 1;
  for (const elementLessonSummary of nElementLessonSummary) {
    const elementLesson = document.createElement('p');
    elementLesson.innerText = `📖 Lesson ${counterLesson}`;
    elementLesson.classList.add('textalign_center');
    elementLessonSummary.prepend(elementLesson);
    counterLesson += 1;
  }
};
