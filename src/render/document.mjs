import { componentExercise } from '../component/exercise.mjs';
import { componentHeading } from '../component/heading.mjs';
import { componentCriterion } from '../component/criterion.mjs';
import { componentGradingcriteria } from '../component/gradingcriteria.mjs';
import { componentIntendedlearningoutcomes } from '../component/intendedlearningoutcomes.mjs';
import { componentMiroboard } from '../component/miroboard.mjs';
import { componentSection } from '../component/section.mjs';
import { componentTableofcontents } from '../component/tableofcontents.mjs';
import { componentVideo } from '../component/video.mjs';
import { renderAll } from './all.mjs';
import { locale } from '../citationlocale.mjs';
import { citationstyleIeee } from '../citationstyle.mjs';
import { processCitations } from '../process_citations.mjs';
import '../css/style.css';
import '../css/biblio.css';

export const renderDocument = citationData => {
  window.addEventListener(
    'DOMContentLoaded',
    () => {
      componentSection(); // TODO: order dependence
      componentTableofcontents();
      componentCriterion();
      componentIntendedlearningoutcomes();
      componentGradingcriteria();
      componentVideo();
      componentMiroboard();
      componentExercise();
      componentHeading();
      if (citationData !== undefined && citationData !== null && citationData.items.length > 0) {
        processCitations(citationData, locale, citationstyleIeee);
      }
      const elementBody = document.body;
      renderAll(elementBody);
    },
    false,
  );
};
