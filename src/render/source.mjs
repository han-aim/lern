export const renderSource = elementBody => {
  const nElementSource = elementBody.querySelectorAll(':scope .source');
  for (const elementBron of nElementSource) {
    elementBron.prepend('ℹ️  ');
  }
};
