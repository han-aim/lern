import Prism from 'prismjs';

import { renderFigurenumbering } from './figurenumbering.mjs';
import { renderMath } from './math.mjs';
import { renderPractice } from './practice.mjs';
import { renderSource } from './source.mjs';
import { renderTablenumbering } from './tablenumbering.mjs';
import { locationHashChanged } from '../fragment_scroller.mjs';

import 'sanitize.css/assets.css';
import 'sanitize.css/reduce-motion.css';
import 'sanitize.css/typography.css';

import 'prismjs/components/prism-java';
import 'prismjs/components/prism-rust';
import 'prismjs/plugins/line-highlight/prism-line-highlight.css';
import 'prismjs/plugins/line-highlight/prism-line-highlight.js';
import 'prismjs/plugins/show-language/prism-show-language.js';
import 'prismjs/plugins/toolbar/prism-toolbar.css';
import 'prismjs/plugins/toolbar/prism-toolbar.js';
import 'prismjs/themes/prism-dark.css';

export const renderAll = elementBody => {
  renderPractice(elementBody);
  renderSource(elementBody);
  renderTablenumbering(elementBody);
  renderFigurenumbering(elementBody);
  Prism.highlightAll();
  renderMath();
  const elementMain = elementBody.querySelector('main');
  if (elementMain !== null) {
    elementMain.hidden = false;
  }
  locationHashChanged();
  window.addEventListener('hashchange', locationHashChanged);
};
