export const renderPractice = elementBody => {
  const nElementPractical = elementBody.querySelectorAll(':scope .practical');
  for (const elementPractical of nElementPractical) {
    elementPractical.prepend('🛠️  ');
  }
};
