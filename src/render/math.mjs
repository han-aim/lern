import { mathjax } from 'mathjax-full/js/mathjax';
import { TeX } from 'mathjax-full/js/input/tex';
import { CHTML } from 'mathjax-full/js/output/chtml';
import { AllPackages } from 'mathjax-full/js/input/tex/AllPackages';
import { browserAdaptor } from 'mathjax-full/js/adaptors/browserAdaptor';
import { RegisterHTMLHandler } from 'mathjax-full/js/handlers/html';

export const renderMath = () => {
  RegisterHTMLHandler(browserAdaptor());
  const html = mathjax.document(document, {
    InputJax: new TeX({
      packages: AllPackages,
      macros: { require: ['', 1] },
    }),
    OutputJax: new CHTML({
      fontURL: `${import.meta.env.BASE_URL}/font`,
    }),
  });
  // TODO: these settings appear not to work. E.g., the menu does not work.
  window.MathJax = {
    version: mathjax.version,
    html,
    options: {
      enableMenu: true,
      menuOptions: {
        settings: {
          texHints: true,
          semantics: false,
          zoom: 'NoZoom',
          zscale: '200%',
          renderer: 'CHTML',
          alt: false,
          cmd: false,
          ctrl: false,
          shift: false,
          scale: 1,
          collapsible: false,
          inTabOrder: true,
        },
        annotationTypes: {
          TeX: ['TeX', 'LaTeX', 'application/x-tex'],
          StarMath: ['StarMath 5.0'],
          Maple: ['Maple'],
          ContentMathML: ['MathML-Content', 'application/mathml-content+xml'],
          OpenMath: ['OpenMath'],
        },
      },
    },
  };
  html.findMath().compile().getMetrics().typeset().updateDocument();
};
